* git branching model

** 0


| 10 | master |
|    |        |
| 20 | debian |
|    |        |
| 30 |        |


** 10


| master |   |
|        |   |
|        |   |


** 20

|               |         |          | before 202107     |               chromium |
|               |         |          |                   | [2020-07-29 Wed 10:16] |
|               |         |          |                   |                        |
|               |         |          |                   |                        |
| wheezy-slim   | 7-slim  |          | obsolete stable   |                        |
|               |         |          |                   |                        |
| jessie-slim   | 8-slim  | 20150425 | oldoldstable-slim |                     57 |
|               |         |          |                   |                        |
| stretch-slim  | 9-slim  | 20170617 | oldstable-slim    |                     73 |
|               |         |          |                   |                        |
| buster-slim   | 10-slim | 20190706 | stable-slim       |                     83 |
|               |         |          |                   |                        |
| bullseye-slim | 11-slim | 202106 ? | testing-slim      |                     83 |
|               |         |          |                   |                        |
| sid-slim      |         |          | unstable-slim     |                     83 |
|               |         |          |                   |                        |


https://en.wikipedia.org/wiki/Debian_version_history


https://hub.docker.com/_/debian?tab=description&page=1&ordering=-name


https://www.debian.org/releases/


https://packages.debian.org/search?keywords=chromium&searchon=names&suite=all&section=all


* master

** 0


| 10 |                    |
|    |                    |
| 20 | local build        |
|    |                    |
| 22 | local docker build |
|    |                    |
| 30 |                    |
|    |                    |


** 20


#  d=debian:stable-slim


#  docker pull $d


docker run -it --rm \
-v /tmp:/tmp \
--userns=host    --privileged \
$d  


xhost +local:
n=my-network
s=/mnt/local/chrome.json
f=/mnt/local/fonts2019/noto-2019


docker run -it --rm \
    --name test  \
    -v /tmp:/tmp \
    -v /mnt:/mnt \
    -e DISPLAY=$DISPLAY \
    --shm-size=1gb \
    --security-opt seccomp=$s \
    --net $n \
    -w /tmp \
    -v $f:/usr/share/fonts/myfonts \
    -e LANG=C.UTF-8 \
    $d



export DEBIAN_FRONTEND=noninteractive


apt update && apt install -y \
chromium \
&& apt clean


    chromium   \
    --user-data-dir=/mnt/zram1/data2 \
    --disable-gpu   \
    --no-sandbox \
    --enable-logging=stderr --v=1 


https://advancetechtutorial.blogspot.com/2018/11/how-solve-x-error-badaccess-attempt-to.html

QT_X11_NO_MITSHM=1


** 22


| tangle a Dockerfile |
|                     |
| docker build        |
|                     |
| docker run          |


f=/mnt/zram1/chromium202008/Dockerfile


docker build -f $f -t chromium-test .


#  docker images | grep chromium-test


#  rm -rf /mnt/zram1/data2


xhost +local:
n=my-network
s=/mnt/local/chrome.json
f=/mnt/local/fonts2019/noto-2019


docker run -it --rm \
    --name test  \
    -v /tmp:/tmp \
    -v /mnt:/mnt \
    -e DISPLAY=$DISPLAY \
    --shm-size=1gb \
    --security-opt seccomp=$s \
    --net $n \
    -w /tmp \
    -v $f:/usr/share/fonts/myfonts \
    -e LANG=C.UTF-8 \
    -e QT_X11_NO_MITSHM=1 \
    -u 1000:1000 \
    chromium-test \
    chromium   \
    --user-data-dir=/mnt/zram1/data2 \
    --disable-gpu   \
    --enable-logging=stderr --v=1 


** 30


#+HEADER: :tangle Dockerfile
#+BEGIN_SRC sh


FROM debian:stable-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -y \
    chromium \
    && apt clean


#+END_SRC
